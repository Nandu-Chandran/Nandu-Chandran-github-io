---
title: "Hello World"
categories:
  - Blog
tags:
  - chat
---
Thanks for visiting my website. This is my very first blog post ever. 

I intend to jot down things I've been passionate about throughout my life. I've always had this notion of getting my mind curious over many things. This includes me fiddling around as an electronic hobbyist or as someone searching the midnight sky for shooting stars.

This will be a place for all those curiosities lurking within me. So consider this blog as a playbook of ideas where I can allow my thoughts to run wild and bring out something useful down the line.

I believe that life is a series of experiments. And you have to learn and correct yourself along the way. I hope what I put out there can be of personal benefit to you. 

Let's see where this goes. Peace!
