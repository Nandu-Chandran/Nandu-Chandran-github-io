---
title: About
icon: fas fa-info
order: 5
---

Welcome to my corner on the internet. I share thoughts on things I'm passionate about like technology, philosophy, and books. 

I fill up my time reading books, tinkering with code, lots of music and movies. I'm on the journey to becoming better by exploring old wisdom, new ideas and sharing what I learn. I find myself ecletic by nature, so consider this as a playbook of great ideas I come across to lead an examined life.   

Check out [what I'm up to right now](https://nandu-chandran.github.io/now).

I enjoy geeking out and having deep conversations. If you have an movie or book recommendation or just want to say hi, [hit me up](https://nandu-chandran.github.io/contact)

